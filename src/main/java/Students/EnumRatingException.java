package Students;

public enum EnumRatingException {
    WRONG_GROUP, NO_STUDENT;
}
