package Students;

import java.util.*;
import java.util.TreeMap;
import java.util.TreeSet;

public class Rows {

    private class DataRating implements Comparable<DataRating>{
        private Date date;
        private Rating rating;

        public DataRating(Date date, Rating rating) {
            this.date = date;
            this.rating = rating;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public Rating getRating() {
            return rating;
        }

        public void setRating(Rating rating) {
            this.rating = rating;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof DataRating)) return false;
            DataRating that = (DataRating) o;
            return Objects.equals(getDate(), that.getDate()) &&
                    getRating() == that.getRating();
        }

        @Override
        public int hashCode() {
            return Objects.hash(getDate(), getRating());
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer();
            sb.append(date.getDate());
            sb.append(":");
            sb.append(date.getMonth());
            sb.append(" - ");
            sb.append(rating);
            return sb.toString();
        }

        @Override
        public int compareTo(DataRating o) {
            return date.compareTo(o.getDate());
        }
    }

    private Map<String,Set<DataRating>> marks;

    public Rows(){marks = new TreeMap<>();
    }

    public void add(String subject,Date date,Rating rating){
        if(!marks.containsKey(subject)){
            marks.put(subject, new TreeSet<DataRating>());
        }
        marks.get(subject).add(new DataRating(date,rating));
    }
    public List<Rating> getRatings(String subject){
        List<Rating> ratings = new ArrayList<>();
        for(DataRating dataRating : marks.get(subject)){
            ratings.add(dataRating.getRating());
        }
        return ratings;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        for(String s : marks.keySet()) {
            sb.append(" | ");
            for (DataRating dataRating : marks.get(s)) {
                sb.append(dataRating);
                sb.append(";");
            }
        }
        return sb.toString();
    }




}

