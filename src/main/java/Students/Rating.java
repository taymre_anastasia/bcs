package Students;

import Students.Mark;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Rating {
    private String subject;
    private String tutorName;
    private Set <String> group;
    private Date date;
    private String year;
    private int semestr;
    private TreeMap<Student, Mark> marks;

    public Rating(String subject, String tutorName, Date date, String year, int semestr, Set<String> group) {
        this.subject = subject;
        this.tutorName = tutorName;
        this.date = date;
        this.year = year;
        this.semestr = semestr;
        this.group = group;
        this.marks = new TreeMap <Student, Mark>();
    }

    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public String getTutorName() {
        return tutorName;
    }
    public void setTutorName(String tutorName) {
        this.tutorName = tutorName;
    }
    public Set<String> getGroup() {
        return group;
    }
    public void setGroup(Set<String> group) {
        this.group = group;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public String getYear() {
        return year;
    }
    public void setYear(String year) {
        this.year = year;
    }
    public int getSemestr() {
        return semestr;
    }
    public void setSemestr(int semestr) {
        this.semestr = semestr;
    }

    public Map<Student,Mark> getRecords(){
        Map <Student, Mark> resultMap = new TreeMap<Student, Mark>();
        for (Student student : marks.keySet()) {
            resultMap.put(new Student(student), marks.get(student));
        }
        return resultMap;
    }

    public void addRecord (Student student, Mark mark) throws RatingException {
        if (!group.isEmpty() && !group.contains(student.getGroup()))
            throw new RatingException(EnumRatingException.WRONG_GROUP);
        marks.put(student, mark);
    }

    public void deleteRecords(Student student){
        marks.remove(student);
    }

    public void changeRating (Student student, Mark newmark) throws RatingException{
        if (!marks.containsKey(student)) throw new RatingException(EnumRatingException.NO_STUDENT);
        marks.put(student,newmark);
    }

    @Override
    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append("Ведомость по дисциплине: ");
        s.append(subject);
        s.append("\n");
        s.append("Дата: ");
        s.append(date);
        s.append("Семестр: ");
        s.append(semestr);
        s.append("Учебный год");
        s.append(year);
        s.append("Преподаватель: ");
        s.append(tutorName);
        s.append("\n");
        s.append("----------------------------------------");
        for(Student student : marks.keySet()){
            s.append("\n");
            s.append(student);
            s.append("-");
            s.append(marks.get(student));
        }
        s.append("\n----------------------------------------");
        return s.toString();
    }

    public void deleteRecord(Student student4) {

    }
}
