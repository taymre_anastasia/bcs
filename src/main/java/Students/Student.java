package Students;

public class Student implements Comparable<Student> {

    private String fullName;
    private String faculty;
    private String group;
    private int id;

    public Student(String fullName, String faculty, String group, int id) {
        this.fullName = fullName;
        this.faculty = faculty;
        this.group = group;
        this.id = id;
    }

    public Student(Student student){
        this(student.fullName, student.faculty, student.group, student.id);
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int compareTo(Student student) {
        return this.getFullName().compareTo(student.getFullName());
    }

    @Override
    public String toString() {
        return "Student{" +
                "fullName='" + fullName + '\'' +
                ", faculty='" + faculty + '\'' +
                ", group='" + group + '\'' +
                ", id=" + id +
                '}';
    }
}
