package Students;

import java.util.*;

public class ControlRating {

    private Set<Rating> rating;
    private String year;
    private int semestr;
    private String group;
    private Map<Student,Rows> table;
    private Set<String> subejcts;

    public ControlRating(String year, int semestr, String group, TreeSet<Rating> rating) {
        this.rating = rating;
        this.year = year;
        this.semestr = semestr;
        this.group = group;
        table = new TreeMap<>();
        subejcts = new TreeSet<>();
    }

    public Set<Rating> getStatements() {
        return rating;
    }

    public void addStatement(Rating rat){
        if(isCorrectRating(rat)){
            rating.add(rat);
            this.build();
        }
    }
    public void deleteRating(Rating statement){
        rating.remove(statement);
        this.build();
    }


    public void setStatements(Set<Rating> rating) {
        this.rating = rating;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getSemestr() {
        return semestr;
    }

    public void setSemestr(int semestr) {
        this.semestr = semestr;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }


    public boolean isCorrectRating(Rating rating){
        return rating.getSemestr() == getSemestr()
                && rating.getYear().equals(getYear())
                &&
                (rating.getGroup().contains(getGroup()) || rating.getGroup().isEmpty());
    }


    public boolean isCorrectStudent(Student student){
        return student.getGroup().equals(getGroup());
    }

    public List<Rating> getRatings(Student student, String subject){
        return table.get(student).getRatings(subject);
    }

    public Set<String> getSubejcts(){
        return new TreeSet<>(subejcts) ;
    }

    public Set<Student> getStudents(){
        return new TreeSet<>(table.keySet());
    }


    public void build(){
        for(Rating statement : rating){
            if(isCorrectRating(statement)){
                subejcts.add(statement.getSubject());
                for(Student student : statement.getRecords().keySet()){
                    if(isCorrectStudent(student)){
                        if(!table.containsKey(student)){
                            table.put(student,new Rows());
                        }
                        table.get(student)
                                .add(statement.getSubject(),statement.getDate(),statement.getRecords().get(student));
                    }
                }
            }
        }
    }



    @Override
    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append("Сводная ведомость группы:");
        s.append(group);
        s.append("  За учебный год:");
        s.append(year);
        s.append("  Семестр:");
        s.append(semestr);
        s.append("\n");
        s.append("------------------------------------------------------");
        s.append("\n");
        s.append("| № | ФИО | ");
        for(String subject : subejcts){
            s.append(subject);
            s.append(" | ");
        }
        int count = 0;

        for(Student student : table.keySet()){
            s.append("\n");
            s.append(" | ");
            s.append(++count);
            s.append(" | ");
            s.append(student.getFullName());
            s.append(table.get(student).toString());
            s.append(" | ");
        }
        s.append("\n");
        s.append("------------------------------------------------------");
        return s.toString();
    }

}
