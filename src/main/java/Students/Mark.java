package Students;

public enum Mark {
    BAD("2"),MEDIUM("3"),GOOD("4"),COOL("5"),
    PASS("Зачет"),FAIL("Незачет"),ABSENCE("Неявка"),NOT("Нет оценки");

    private String message;
    Mark(String message){
        this.message = message;
    }
    @Override
    public String toString() {
        return message;
    }
}
