package Students;

import org.junit.Test;

import java.beans.Statement;
import java.util.Date;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.util.*;
import static org.junit.Assert.*;

public class RatingTest {

    private String discipline = "Base comp science";
    private String nameLecture = "Ashaev I.V.";
    private int semestr = 8;
    private int year = 2019;
    private Set<String> groups = new HashSet<>();
    private Date date = new Date();



    private Student student1 = new Student("Иван","IMIT","MMS",1);
    private Student student2 = new Student("Петр","FKN","LOL", 2);
    private Student student3 = new Student("Мария","IMIT","MMS", 3);
    private Student student4 = new Student("Илья","FilFak","FFK", 4);



    public Rating createRating(){
        return new Rating(discipline,nameLecture,date,semestr,year,groups);
    }

    @Test
    public void testGet(){
        Rating statement = createRating();

        assertEquals(discipline,statement.getSubject());
        assertEquals(nameLecture,statement.getTutorName());
        assertEquals(semestr,statement.getSemestr());
        assertEquals(date,statement.getDate());
        assertEquals(year,statement.getYear());

        Set<String> getGroups = statement.getGroup();
        Map<Student, Rating> records = statement.getRecords();

        assertNotNull(getGroups);
        assertNotNull(records);
        assertTrue(getGroups.isEmpty());
        assertTrue(records.isEmpty());

    }

    @Test
    public void testShow() throws RatingException {
        Rating statement = createRating();
        statement.addRecord(student1,Mark.COOL);
        statement.addRecord(student2,Mark.FAIL);
        statement.addRecord(student3,Mark.ABSENCE);
        System.out.println(statement);
    }

    @Test
    public void testAddCorectWithoutGroups() throws RatingException {
        Rating statement = createRating();

        statement.addRecord(student1,Mark.COOL);
        statement.addRecord(student2,Mark.FAIL);
        statement.addRecord(student3,Mark.ABSENCE);

        testCheckRecords(statement);

    }

    @Test
    public void testAddCorrectWithGroups() throws RatingException {
        Rating statement = createRating();
        Set<String> groups = new HashSet<>();
        groups.add("MMS");
        groups.add("LOL");
        groups.add("AAA");
        statement.setGroup(groups);

        statement.addRecord(student1,Mark.COOL);
        statement.addRecord(student2,Mark.FAIL);
        statement.addRecord(student3,Mark.ABSENCE);

        testCheckRecords(statement);

    }

    @Test(expected = RatingException.class)
    public void testAddWrong() throws RatingException {
        Rating statement = createRating();
        Set<String> groups = new HashSet<>();
        groups.add("MMS");
        groups.add("LOL");
        groups.add("AAA");
        statement.setGroup(groups);

        statement.addRecord(student1,Mark.COOL);
        statement.addRecord(student2,Mark.FAIL);
        statement.addRecord(student3,Mark.ABSENCE);
        statement.addRecord(student4,Mark.NOT);

        testCheckRecords(statement);
    }

    public void testCheckRecords(Rating statement){
        Map<Student,Rating> resultMap = statement.getRecords();
        assertEquals(3,resultMap.size());

        assertTrue(resultMap.containsKey(student1));
        assertTrue(resultMap.containsKey(student2));
        assertTrue(resultMap.containsKey(student3));

        assertEquals(Mark.COOL,resultMap.get(student1));
        assertEquals(Mark.FAIL,resultMap.get(student2));
        assertEquals(Mark.ABSENCE,resultMap.get(student3));
    }

    @Test
    public void testDelete() throws RatingException {
        Rating statement = createRating();

        statement.addRecord(student1,Mark.COOL);
        statement.addRecord(student2,Mark.FAIL);
        statement.addRecord(student3,Mark.ABSENCE);
        statement.addRecord(student4,Mark.ABSENCE);

        assertEquals(4,statement.getRecords().size());

        statement.deleteRecord(student4);

        testCheckRecords(statement);
    }

    @Test
    public void testChangeCorrect() throws RatingException {
        Rating statement = createRating();

        statement.addRecord(student1,Mark.COOL);
        statement.addRecord(student2,Mark.NOT);
        statement.addRecord(student3,Mark.ABSENCE);

        statement.changeRating(student2,Mark.FAIL);

        testCheckRecords(statement);

    }

    @Test(expected = RatingException.class)
    public void testChangeWrong() throws RatingException {
        Rating statement = createRating();

        statement.addRecord(student1,Mark.COOL);
        statement.addRecord(student2,Mark.NOT);
        statement.addRecord(student3,Mark.ABSENCE);

        statement.changeRating(student4,Mark.FAIL);
    }

    @Test
    public void addStudentTwo() throws RatingException {
        Rating statement = createRating();

        statement.addRecord(student1,Mark.COOL);
        statement.addRecord(student1,Mark.FAIL);

        assertEquals(1,statement.getRecords().size());
    }

    @Test
    public void testCorrectGetRecords() throws RatingException {
        Rating statement = createRating();

        statement.addRecord(student1,Mark.COOL);

        Map<Student,Rating> resultMap = statement.getRecords();

        resultMap.remove(student1);

        assertEquals(0,resultMap.size());
        assertEquals(1,statement.getRecords().size());

        Student resultStudent = statement.getRecords().keySet().iterator().next();

        assertEquals(resultStudent,student1);

        resultStudent.setFullName("Another");

        assertEquals("Another",resultStudent.getFullName());
        assertEquals("Иван",student1.getFullName());

    }

    @Test
    public void testCheckAlphabetOrder() throws RatingException {
        Rating statement = createRating();

        statement.addRecord(student1,Mark.COOL);
        statement.addRecord(student2,Mark.COOL);
        statement.addRecord(student3,Mark.COOL);
        statement.addRecord(student4,Mark.COOL);

        Map<Student,Rating> resultRecords = statement.getRecords();

        Iterator<Student> iter = resultRecords.keySet().iterator();
        assertEquals("Иван",iter.next().getFullName());
        assertEquals("Илья",iter.next().getFullName());
        assertEquals("Мария",iter.next().getFullName());
        assertEquals("Петр",iter.next().getFullName());

    }





}