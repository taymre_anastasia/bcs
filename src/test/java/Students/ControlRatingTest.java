package Students;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class ControlRatingTest {


    private ControlRating summaryStatementMMS;
    private ControlRating summaryStatementMPB;

    @Before
    public void beforeTest(){
        List<ControlRating> summaryStatements = createControlRating();
        summaryStatementMMS = summaryStatements.get(0);
        summaryStatementMPB = summaryStatements.get(1);
        group.add("MMS");
        group.add("MPB");

    }

    private static final String YEAR = "2019";
    private static final int SEMESTR = 8;
    private static final String MMS = "ММС-601-0";
    private static final String MPB = "МПБ-601-0";
    private static final Set<String> group = new HashSet<String>();


    private static final String FACULTY = "ИМИТ";
    private static Student student11 = new Student("Адельшин",FACULTY,MMS,1);
    private static  Student student12 = new Student("Таймре",FACULTY,MMS,2);
    private static  Student student13 = new Student("Садуллаева",FACULTY,MMS,3);
    private static  Student student14 = new Student("Шипицин",FACULTY,MMS,4);

    private Student student21 = new Student("Козлова",FACULTY,MPB,5);
    private static  Student student22 = new Student("Ибрагимова",FACULTY,MPB,6);
    private static  Student student23 = new Student("Ковалев",FACULTY,MPB,7);
    private static  Student student24 = new Student("Козуев",FACULTY,MPB,8);
    private static  Student student25 = new Student("Ющенко",FACULTY,MPB,9);

    private static  String subjectBaseCompScience = "Основы компьютерных наук";
    private static  String subjectMathAnalZachet= "Математический анализ зачет";
    private static  String subjectMathAnalExam = "Математический анализ экзамен";
    private static  String subjectPrograming = "Программирование";

    private static  String ASHAEV = "Ашаев Игорь Викторович";
    private static  String MELNIKOV = "Мельников Евгений Владимирович";

    private static Rating statementBaseCopmScience =
            new Rating(subjectBaseCompScience,ASHAEV,new Date(2019,9,18),YEAR,SEMESTR, group);

    private static Rating statementPrograming =
            new Rating(subjectPrograming,ASHAEV,new Date(2019,9,18),YEAR,SEMESTR,group);

    private static Rating statementMathAnalZachet1 =
            new Rating(subjectMathAnalZachet,MELNIKOV,new Date(2019,9,19),YEAR,SEMESTR,group);

    private static Rating statementMathAnalZachet2 =
            new Rating(subjectMathAnalZachet,MELNIKOV,new Date(2019,10,1),YEAR,SEMESTR,group);

    private static Rating statementMathAnalExam1 =
            new Rating(subjectMathAnalExam,MELNIKOV,new Date(2019,9,20),YEAR,SEMESTR,group);

    private static Rating statementMathAnalExam2 =
            new Rating(subjectMathAnalExam,MELNIKOV,new Date(2019,10,10),YEAR,SEMESTR,group);

    private static Rating statementMathAnalExam3 =
            new Rating(subjectMathAnalExam,MELNIKOV,new Date(2019,10,15),YEAR,SEMESTR,group);


    public List<ControlRating> createControlRating(){
        List<ControlRating> summaryStatements = new ArrayList<>();
        try {
            statementBaseCopmScience.addRecord(student11, Mark.COOL);
            statementBaseCopmScience.addRecord(student12,Mark.GOOD);
            statementBaseCopmScience.addRecord(student13,Mark.COOL);
            statementBaseCopmScience.addRecord(student14,Mark.GOOD);

            statementPrograming.addRecord(student21,Mark.COOL);
            statementPrograming.addRecord(student22,Mark.COOL);
            statementPrograming.addRecord(student23,Mark.COOL);
            statementPrograming.addRecord(student24,Mark.GOOD);
            statementPrograming.addRecord(student25,Mark.MEDIUM);

            statementMathAnalZachet1.addRecord(student11,Mark.PASS);
            statementMathAnalZachet1.addRecord(student12,Mark.PASS);
            statementMathAnalZachet1.addRecord(student13,Mark.PASS);
            statementMathAnalZachet1.addRecord(student14,Mark.ABSENCE);
            statementMathAnalZachet1.addRecord(student21,Mark.PASS);
            statementMathAnalZachet1.addRecord(student22,Mark.PASS);
            statementMathAnalZachet1.addRecord(student23,Mark.PASS);
            statementMathAnalZachet1.addRecord(student24,Mark.FAIL);
            statementMathAnalZachet1.addRecord(student25,Mark.PASS);

            statementMathAnalExam1.addRecord(student11,Mark.COOL);
            statementMathAnalExam1.addRecord(student12,Mark.COOL);
            statementMathAnalExam1.addRecord(student13,Mark.COOL);
            statementMathAnalExam1.addRecord(student14,Mark.ABSENCE);
            statementMathAnalExam1.addRecord(student21,Mark.MEDIUM);
            statementMathAnalExam1.addRecord(student22,Mark.COOL);
            statementMathAnalExam1.addRecord(student23,Mark.GOOD);
            statementMathAnalExam1.addRecord(student24,Mark.BAD);
            statementMathAnalExam1.addRecord(student25,Mark.GOOD);

            statementMathAnalZachet2.addRecord(student14,Mark.PASS);
            statementMathAnalZachet2.addRecord(student24,Mark.PASS);

            statementMathAnalExam2.addRecord(student24,Mark.BAD);
            statementMathAnalExam2.addRecord(student14,Mark.GOOD);

            statementMathAnalExam3.addRecord(student24,Mark.MEDIUM);

            TreeSet<Rating> statements = new TreeSet<>();
            statements.add(statementBaseCopmScience);
            statements.add(statementPrograming);
            statements.add(statementMathAnalExam1);
            statements.add(statementMathAnalExam2);
            statements.add(statementMathAnalExam3);
            statements.add(statementMathAnalZachet1);
            statements.add(statementMathAnalZachet2);

            ControlRating summaryStatementMMS =
                    new ControlRating(YEAR,SEMESTR,MMS,statements);

            summaryStatementMMS.build();

            ControlRating summaryStatementMPB =
                    new ControlRating(YEAR,SEMESTR,MPB,statements);

            summaryStatementMPB.build();

            summaryStatements.add(summaryStatementMMS);
            summaryStatements.add(summaryStatementMPB);

        } catch (RatingException e) {
            e.printStackTrace();
        }
        return summaryStatements;
    }

    @Test
    public void testShow(){
        System.out.println(summaryStatementMMS);
        System.out.println(summaryStatementMPB);
    }

    @Test
    public void testGetRating(){
        List<Rating> ratingList = summaryStatementMPB.getRatings(student24,subjectMathAnalExam);
        assertEquals(3,ratingList.size());
        assertEquals(Mark.BAD,ratingList.get(0));
        assertEquals(Mark.BAD,ratingList.get(1));
        assertEquals(Mark.MEDIUM,ratingList.get(2));
    }

    @Test
    public void testAddStatement(){
        String subject = "Физкультура";
        String nameLecture = "Физрук";
        Rating newStatement = new Rating(subject, nameLecture, new Date(2019,1,11),YEAR,SEMESTR,group);
        try {
            newStatement.addRecord(student12,Mark.COOL);
            newStatement.addRecord(student13,Mark.COOL);
            summaryStatementMMS.addStatement(newStatement);
            System.out.println(summaryStatementMMS);
        } catch (RatingException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testGetSubjects(){
        Set<String> subjectSet = summaryStatementMMS.getSubejcts();
        assertEquals(3,subjectSet.size());
        assertTrue(subjectSet.contains(subjectBaseCompScience));
        assertTrue(subjectSet.contains(subjectMathAnalExam));
        assertTrue(subjectSet.contains(subjectMathAnalZachet));
    }

    @Test
    public void testGetStudenst(){
        Set<Student> students = summaryStatementMMS.getStudents();
        assertEquals(4,students.size());
        assertTrue(students.contains(student11));
        assertTrue(students.contains(student12));
        assertTrue(students.contains(student13));
        assertTrue(students.contains(student14));
    }

    @Test
    public void testBuild(){
        System.out.println(summaryStatementMMS);
        summaryStatementMMS.build();
        System.out.println(summaryStatementMMS);
    }


}